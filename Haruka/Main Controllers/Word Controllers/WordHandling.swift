//
//  WordHandling.swift
//  Haruka
//
//  Created by Alfredo Uzumaki on 4/16/20.
//  Copyright © 2020 Alfredo Uzumaki. All rights reserved.
//

import Foundation

private func processQuestion(sentences: Sentence) {
    print("- question: " + sentences.words.compactMap({$0.name}).joined(separator: " "))
    print("Haruka: OK I will answer that later")
}

private func processOrder(sentences: Sentence) {
    print("- order: " + sentences.words.compactMap({$0.name}).joined(separator: " "))
//    print("- order rank: \(rankOrder(sentences.words.first!))")
    if orderWorthDoing(sentences.words.first!.totalFeel) {
        doThis(sentences.words.last!)
    } else {
        print("Haruka: Sorry I can't do this")
    }
}

private func processNews(sentences: Sentence) {
    print("- news: " + sentences.words.compactMap({$0.name}).joined(separator: " "))
    print("Haruka: Good to know!")
}

private func processRequest(sentences: Sentence) {
    print("- request: " + sentences.words.compactMap({$0.name}).joined(separator: " "))
    print("Haruka: let me think.")
}

private func convertWordsToSentence(words: [Word])-> Sentence {
    var sentence = Sentence(words: words, type: .news)
    if words.first!.type == .exclamation || words.compactMap({$0.name}).contains("?") {
        sentence.type = .question
    } else if words.first!.type == .noun {
        sentence.type = .request
    } else if words.first!.type == .verb {
        sentence.type = .order
    } else {
        sentence.type = .news
    }
    return sentence
}

func processWords(_ words: [Word]) {
    let sentence = convertWordsToSentence(words: words)
    switch sentence.type {
    case .question: processQuestion(sentences: sentence)
    case .order: processOrder(sentences: sentence)
    case .news: processNews(sentences: sentence)
    case .request: processRequest(sentences: sentence)
    }
}

func orderWorthDoing(_ rank: Int)-> Bool {
    if rank < 0 {
        print("Haruka: Its not worth doing")
        return false
    } else {
        print("Haruka: It should be done!")
        return true
    }
}

func doThis(_ word: Word) {
    print("word: \(word.name!) | valid words: \(words.compactMap({$0.name}))")
    if words.compactMap({$0.name}).contains(word.name) {
        print("Haruka: I will do this right now")
    } else {
        print("Haruka: I can't do this right now, mabe in future")
    }
}
