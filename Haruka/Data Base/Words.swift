//
//  Words.swift
//  Haruka
//
//  Created by Alfredo Uzumaki on 4/16/20.
//  Copyright © 2020 Alfredo Uzumaki. All rights reserved.
//

// some of words should store on long memory & some of them should on short memory

import Foundation

let aboutSelfWords = ["are you", "do you"]
let requestWorks = ["can you", "could you", "would you"]
let orderWorks = ["can you", "could you", "would you", "how"]
let questionWords = ["how", "what", "Why", "Where", "When", "Who"]

let words = [
    Word(name: "Weather", type: .noun),
]


