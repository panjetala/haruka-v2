//
//  Sentence.swift
//  Haruka
//
//  Created by Alfredo Uzumaki on 4/16/20.
//  Copyright © 2020 Alfredo Uzumaki. All rights reserved.
//

import Foundation

struct Sentence {
    var words:[Word]
    var type: SentenceType
}

enum SentenceType {
    case question
    case order
    case news
    case request
}
