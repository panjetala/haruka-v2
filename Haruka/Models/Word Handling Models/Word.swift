//
//  Word.swift
//  Haruka
//
//  Created by Alfredo Uzumaki on 4/16/20.
//  Copyright © 2020 Alfredo Uzumaki. All rights reserved.
//

import Foundation

struct Word {
    
    var name: String!
    var type: WordType!
    var family = [String]()
    var goodness = 0
    var badness = 0
    var totalFeel = 0
    
    init(name:String, type: WordType, goodness:Int = 0, badness:Int = 0, family:[String] = []) {
        self.name = name
        self.family = family
        self.type = type
        self.goodness = goodness
        self.badness = badness
        self.totalFeel = goodness - badness
    }
}

enum WordType {
    case noun // A noun is a word that identifies: a person (man, girl, engineer, friend) a thing (horse, wall, flower, country) an idea, quality, or state (anger, courage, life, luckiness)
    case verb // A verb describes what a person or thing does or what happens. For example, verbs describe: an action – jump, stop, explore an event – snow, happen a situation – be, seem, have a change – evolve, shrink, widen
    case adjective // An adjective is a word that describes a noun, giving extra information about it. For example: an exciting adventure a green apple a tidy room
    case adverb // An adverb is a word that’s used to give information about a verb, adjective, or other adverb. They can make the meaning of a verb, adjective, or other adverb stronger or weaker, and often appear between the subject and its verb (She nearly lost everything.)
    case pronoun // Pronouns are used in place of a noun that is already known or has already been mentioned. This is often done in order to avoid repeating the noun. For example: Laura left early because she was tired. Anthony brought the avocados with him. That is the only option left. Something will have to change.
    case preposition // A preposition is a word such as after, in, to, on, and with. Prepositions are usually used in front of nouns or pronouns and they show the relationship between the noun or pronoun and other words in a sentence. They describe, for example, the position of something, the time when something happens, or the way in which something is done.
    case conjunction // A conjunction (also called a connective) is a word such as and, because, but, for, if, or, and when. Conjunctions are used to connect phrases, clauses, and sentences.The two main kinds are known as coordinating conjunctions and subordinating conjunctions.
    case determiner // A determiner is a word that introduces a noun, such as a/an, the, every, this, those, or many (as in a dog, the dog, this dog, those dogs, every dog, many dogs). The determiner the is sometimes known as the definite article and the determiner a (or an) as the indefinite article.
    case exclamation // An exclamation (also called an interjection) is a word or phrase that expresses strong emotion, such as surprise, pleasure, or anger. Exclamations often stand on their own, and in writing they are usually followed by an exclamation mark rather than a full stop.
}
