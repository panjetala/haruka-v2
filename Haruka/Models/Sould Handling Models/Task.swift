//
//  Task.swift
//  Haruka
//
//  Created by Alfredo Uzumaki on 4/16/20.
//  Copyright © 2020 Alfredo Uzumaki. All rights reserved.
//

import Foundation

struct Task {
    
    var id = 0
    var priority = 0 // 1 ~ 10
    var durationType: TaskDurationType!
    var deadline:Date?
    var startData = Date()
    var title = ""
    
    
    enum TaskDurationType { case inSecond, inMinute, inMinutes, inHour, inHours, inDay, inWeek, inMonth, inYear, inLife}
}
